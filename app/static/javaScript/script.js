function getName(botao){
    if (botao == "audioButton"){
        getNameVideo()
        document.getElementById('inicio').innerHTML = 'Carregando vídeo...'
        defDownload(getOs(), 'audio');
    } else if(botao == "videoButton"){
        getNameVideo()
        document.getElementById('inicio').innerHTML = 'Carregando vídeo...'
        defDownload(getOs(), 'video');
    }
}


function defDownload(os, type){
    if(type == 'video'){
        if(os == "Windows"){
            sendData('/downloadVideoWindows')
        }else if(os == "Linux"){
            sendData('/downloadVideoLinux')
        }
    }else if(type == 'audio'){
        if(os == "Windows"){
            sendData('/downloadMusicWindows')
        }else if(os == "Linux"){
            sendData('/downloadMusicLinux')
        }
    }
}


function sendData(url){
    $("#formUrl").on("submit", function(e){
        e.preventDefault();
        var formData = $(this).serialize();
        $.post(url, formData, function(data){
            document.getElementById('resultado').innerHTML = data
        });
    });
}


function getNameVideo(){
    $("#formUrl").on("submit", function(e){
        e.preventDefault();
        var formDataName = $(this).serialize();
        $.post('/getNameVideo', formDataName, function(data){
            getNameVideo();
            document.getElementById('nomeVideo').innerHTML = 'Baixando: ' + data
        });
    });
}


function getOs(){
    var nav = navigator.appVersion;
    if(nav.includes("Windows")){
        return 'Windows'
    }else if(nav.includes("Linux")){
        return 'Linux'
    }
}