from flask import Flask, render_template, request, jsonify
from pytube import YouTube
import getpass

app = Flask(__name__)


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/about', methods=['GET'])
def about():
    return render_template('about.html')


@app.route('/downloadMusicLinux', methods=['POST'])
def downloadMusicLinux():
    path = '/home/'+getpass.getuser()+'/Música'
    link = request.form['link']
    try:
        download(path, link, 'audio')
        return('Download concluido, salvo em %s' %path)
    except Exception as e:
        print('Error: ',e)
        return('Erro ao fazer download :(')


@app.route('/downloadMusicWindows', methods=['POST'])
def downloadMusicWindows():
    path = 'C:/Users/'+getpass.getuser()+'/Downloads'
    link = request.form['link']
    try:
        download(path, link, 'audio')
        return('Download concluido, salvo em %s' %path)
    except Exception as e:
        print('Error: ',e)
        return('Erro ao fazer download :(')


@app.route('/downloadVideoWindows', methods=['POST'])
def downloadVideoWindows():
    path = 'C:/Users/'+getpass.getuser()+'/Downloads'
    link = request.form['link']
    try:
        download(path, link, 'video')
        return('Download concluido, salvo em %s' %path)
    except Exception as e:
        print('Error: ',e)
        return('Erro ao fazer download :(')


@app.route('/downloadVideoLinux', methods=['POST'])
def downloadVideoLinux():
    path = '/home/'+getpass.getuser()+'/Downloads'
    link = request.form['link']
    try:
        download(path, link, 'video')
        return('Download concluido, salvo em %s' %path)
    except Exception as e:
        print('Error: ',e)
        return('Erro ao fazer download :(')



@app.route('/getNameVideo', methods=['POST'])
def getNameVideo():
    link = request.form['link']
    try:
        yt = YouTube(link)
        return yt.title
    except Exception as e:
        print('Error: ',e)
        return('Erro ao carregar vídeo :(')


def download(path, link, type):
    yt = YouTube(link)
    if type == 'video':
        stream = yt.streams.first()
        stream.download(path)
    elif type == 'audio':
        stream = yt.streams.filter(only_audio=True).first()
        stream.download(path)
